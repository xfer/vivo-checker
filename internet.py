# coding: utf-8

import requests
from bs4 import BeautifulSoup

def extrai_planos_site_vivo(cep, numero):
	"""Extrai uma lista de planos do site da vivo. Preciso do CEP e número. Só funciona para apartamentos."""
	base = 'http://www.vivotv.com.br/assine/Monte?origin=sitefibra-assine&cep=%s&numero=%s&imovel=predio&telefone=1121111111&nome=abcbcbcb&optin=true&ofertaInternet=26' % (cep, numero)

	soup = BeautifulSoup(requests.get(base).text, "html.parser")
	return [x.text for x in soup.find_all("div", attrs={"class": "plan-options box internet"})[0].find_all("h2")]

def mostra_plano(plano):
	"""Exibe informações de um plano."""
	print('- %s' % (plano, ))

def mostra_planos(endereco):
	"""Exibe todos os planos de internet disponíveis para uma localidade."""
	nome, cep, numero = endereco
	print('%s' % (nome, ))
	planos = extrai_planos_site_vivo(cep, numero)
	[mostra_plano(plano) for plano in planos]

	print('')

if __name__ == "__main__":
	enderecos = [
		('NOME', 'CEP', 'NUMERO')
	]

	[mostra_planos(endereco) for endereco in enderecos]